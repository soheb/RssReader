RSS Reader
==========

Really basic RSS Reader, fetches Awkward Zombie RSS feeds and then parses them into a list.

How to use
----------

Clone git repo, import into Android Studio as Gradle project, then Run the app.

Tested on Genymotion Nexus 10 and Genymotion Nexus 4 devices.

Libraries used
--------------

[Android RSS Reader Library](https://github.com/matshofman/Android-RSS-Reader-Library)

Some minor tweaks to the Android RSS Reader Library to make it Serialisable instead of Parcellable.



