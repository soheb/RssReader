package io.github.somoso.flexiblefragmentui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import nl.matshofman.saxrssreader.RssItem;

/**
 * A fragment representing a single RssItem detail screen.
 * This fragment is either contained in a {@link RssItemListActivity}
 * in two-pane mode (on tablets) or a {@link RssItemDetailActivity}
 * on handsets.
 */
public class RssItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RssItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rssitem_detail, container, false);

        RssItem item = null;

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            item = (RssItem)getArguments().getSerializable(ARG_ITEM_ID);
        }

        // Show the dummy content as text in a TextView.
        if (item != null) {
            ((TextView) rootView.findViewById(R.id.title)).setText(item.getTitle());
            ((WebView) rootView.findViewById(R.id.rssitem_detail)).loadUrl(item.getLink());
        }

        return rootView;
    }
}
